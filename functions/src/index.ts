// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const json2csv = require("json2csv").parse;

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

// Take the text parameter passed to this HTTP endpoint and insert it into the
// Realtime Database under the path /messages/:pushId/original
exports.report = functions.https.onRequest((req:any, res:any) => {

    const collection = req.query.collection;

    if (collection !== null && collection !== undefined && collection !== 'undefined' && collection !== '') {
        admin.firestore().collection(collection).get().then((snapshot: any) => {
            let values: any = [];
            snapshot.forEach((doc: any) => {
                values.push(doc.data());
              });
    
              const csv = json2csv(values);
              res.setHeader(
                "Content-disposition",
                "attachment; filename=report.csv"
              )
              res.set("Content-Type", "text/csv")
              res.status(200).send(csv);
    
        }).catch((reason: any) => {
            res.status(500, reason);
        });
    } else {
        res.status(500).send("Necessário informar collection através do parametro 'collection'.");
    }
});
  