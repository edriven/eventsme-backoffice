import { CategoriaService } from './../../http/categoria.service';
import { Evento } from './../../model/evento';
import { EventoService } from './../../http/evento.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';
import { MatSnackBar, DateAdapter } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireUploadTask } from '@angular/fire/storage';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cadastro-evento',
  templateUrl: './cadastro-evento.component.html'
})
export class CadastroEventoComponent implements OnInit {

  private formEvento: FormGroup;
  private evento: Evento = new Evento();
  private controleTela: any = {};
  private taskImagePrincipal: AngularFireUploadTask;
  private taskImageDetalhe: AngularFireUploadTask;
  private horas: Array<string> = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
  private minutos: Array<string> = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59'];
  private categorias: Observable<any>;
  private categoriasBase: any;

  constructor(
    private eventoService: EventoService,
    private message: MatSnackBar,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private adapter: DateAdapter<any>,
    private categoriaService: CategoriaService
  ) {
    this.adapter.setLocale('pt-Br');
  }
  ngOnInit() {
    this.formEvento = new FormGroup({
      nome: new FormControl(''),
      categoria: new FormControl(''),
      descricao: new FormControl(''),
      urlPurchase: new FormControl(''),
      urlMarketing: new FormControl(''),
      data: new FormControl(''),
      hora: new FormControl(''),
      minuto: new FormControl(''),
      endereco: new FormControl(''),
      cidade: new FormControl(''),
      uf: new FormControl(''),
      valorHomem: new FormControl(''),
      valorMulher: new FormControl(''),
      dateExhibition: new FormControl(new Date()),
      hourExhibition: new FormControl(''),
      minuteExhibition: new FormControl(''),
      suggested: new FormControl(false),
      showDetails: new FormControl(false),
      active: new FormControl(true),
      proporcaoImagemPrincipal: new FormControl(''),
      proporcaoImagemDetalhe: new FormControl(''),
      showIndex: new FormControl(''),
      marketing: new FormControl(''),
      eventsmeUrl: new FormControl(''),
      eventsmeValue: new FormControl(''),
      partnerUrl: new FormControl(''),
      partnerValue: new FormControl(''),
      stubhubUrl: new FormControl(''),
      stubhubValue: new FormControl(''),
      ticketswapUrl: new FormControl(''),
      ticketswapValue: new FormControl(''),
    });

    this.activatedRoute.params.subscribe(params => {
      this.evento.id = params['id'];
      this.evento.order = parseInt(params['order']);
      if (this.evento.id) {
        this.eventoService.obter(this.evento.id).subscribe(evento => {
          this.evento = evento;
          this.popularFormulario(this.formEvento, evento);
        });
      }
    });
    this.categorias = this.categoriaService.getCategorias();

    this.categorias.subscribe(val => {
      this.categoriasBase = val;
    });
  }

  popularFormulario(form: FormGroup, evento: Evento) {
    form.controls.nome.setValue(evento ? evento.name || '' : '');
    form.controls.categoria.setValue(evento ? evento.category || '' : '');
    form.controls.descricao.setValue(evento ? evento.description || '' : '');
    form.controls.urlPurchase.setValue(evento ? evento.urlPurchase || '' : '');

    form.controls.data.setValue(evento ? new Date(evento.dateTimeBegin) || 0 : 0);
    const hour = evento ? new Date(evento.dateTimeBegin).getHours().toString() : '0';
    form.controls.hora.setValue(evento && (hour.length === 1) ? '0' + hour : hour || '');
    const minute = evento ? new Date(evento.dateTimeBegin).getMinutes().toString() : '0';
    form.controls.minuto.setValue(evento && (minute.length === 1) ? '0' + minute : minute || '');

    form.controls.endereco.setValue(evento ? evento.address || '' : '');
    form.controls.cidade.setValue(evento ? evento.city || '' : '');
    form.controls.uf.setValue(evento ? evento.uf || '' : '');
    form.controls.valorHomem.setValue(evento ? evento.manPrice || '' : '');
    form.controls.valorMulher.setValue(evento ? evento.womanPrice || '' : '');

    form.controls.dateExhibition.setValue(evento ? new Date(evento.dateTimeExhibition) || '' : '');
    const hourExhibition = new Date(evento.dateTimeExhibition).getHours().toString();
    form.controls.hourExhibition.setValue(evento && (hour.length === 1) ? '0' + hourExhibition : hourExhibition || '');
    const minuteExhibition = new Date(evento.dateTimeExhibition).getMinutes().toString();
    form.controls.minuteExhibition.setValue((minuteExhibition.length === 1) ? '0' + minuteExhibition : minuteExhibition || '');

    form.controls.urlMarketing.setValue(evento ? evento.urlMarketing || '' : '');
    form.controls.marketing.setValue(evento ? evento.marketing || false : false);
    form.controls.showIndex.setValue(evento ? evento.showIndex || false : false);
    form.controls.showDetails.setValue(evento ? evento.showDetails || false : false);
    form.controls.suggested.setValue(evento ? evento.suggested || false : false);
    form.controls.active.setValue(evento ? evento.active || false : false);
    form.controls.proporcaoImagemPrincipal.setValue(evento ? evento.principalImageProportion || '' : '');
    form.controls.proporcaoImagemDetalhe.setValue(evento ? evento.detailImageProportion || '' : '');

    //  
    //   edilsonribeiro 10/08/2019
    // 
    form.controls.eventsmeUrl.setValue(evento ? evento.eventsmeUrl || '' : '')
    form.controls.eventsmeValue.setValue(evento ? evento.eventsmeValue || '' : '')
    form.controls.partnerUrl.setValue(evento ? evento.partnerUrl || '' : '');
    form.controls.partnerValue.setValue(evento ? evento.partnerValue || '' : '');
    form.controls.stubhubUrl.setValue(evento ? evento.stubhubUrl || '' : '');
    form.controls.stubhubValue.setValue(evento ? evento.stubhubValue || '' : '');
    form.controls.ticketswapUrl.setValue(evento ? evento.ticketswapUrl || '' : '');
    form.controls.ticketswapValue.setValue(evento ? evento.ticketswapValue || '' : '')
    /**
     * 10/08/2019
     */
  }

  getEvento(form: FormGroup, evento: Evento) {
    const valores = form.value;
    evento.name = valores.nome;
    evento.category = valores.categoria;
    evento.description = valores.descricao;

    const dataEventoComHoraMinuto = new Date(valores.data);
    const dataEvento = new Date(valores.data);
    dataEventoComHoraMinuto.setHours(valores.hora);
    dataEventoComHoraMinuto.setMinutes(valores.minuto);
    evento.dateTimeBegin = dataEventoComHoraMinuto.getTime();
    dataEvento.setHours(0);
    dataEvento.setMinutes(0);
    evento.date = dataEvento.getTime();

    const dateExhibitionComHoraMinuto = new Date(valores.dateExhibition);
    dateExhibitionComHoraMinuto.setHours(valores.hourExhibition);
    dateExhibitionComHoraMinuto.setMinutes(valores.minuteExhibition);
    evento.dateTimeExhibition = dateExhibitionComHoraMinuto.getTime();

    evento.urlMarketing = valores.urlMarketing;
    evento.urlPurchase = valores.urlPurchase;
    evento.address = valores.endereco;
    evento.city = valores.cidade;
    evento.uf = valores.uf;
    evento.manPrice = valores.valorHomem;
    evento.womanPrice = valores.valorMulher;
    evento.marketing = valores.marketing;
    evento.showDetails = valores.showDetails;
    evento.showIndex = valores.showIndex;
    evento.suggested = valores.suggested;
    evento.active = valores.active;
    evento.principalImageProportion = valores.proporcaoImagemPrincipal;
    evento.detailImageProportion = valores.proporcaoImagemDetalhe;

    evento.eventsmeUrl = valores.eventsmeUrl;
    evento.eventsmeValue = valores.eventsmeValue;
    evento.partnerUrl = valores.partnerUrl;
    evento.partnerValue = valores.partnerValue;
    evento.stubhubUrl = valores.stubhubUrl;
    evento.stubhubValue = valores.stubhubValue;
    evento.ticketswapUrl = valores.ticketswapUrl;
    evento.ticketswapValue = valores.ticketswapValue;


    return evento;
  }

  update() {
    let invalido = false;
    /* if (!this.formEvento.valid) {
      this.message.open('Preencha todos os campos obrigatórios', 'Ok', {duration: 5000});
      invalido = true;
    } */

    /* if ((!this.evento.downloadUrlImagePrincipal) && (!this.controleTela.imagemPrincipal)) {
      this.message.open('Uma imagem principal deve ser anexada', 'Ok', {duration: 5000});
      invalido = true;
    }

    if ((!this.evento.downloadUrlImageDetail) && (!this.controleTela.imagemDetalhe)) {
      this.message.open('Uma imagem de detalhe deve ser anexada', 'Ok', {duration: 5000});
      invalido = true;
    } */

    if (invalido) {
      return;
    }

    if (this.controleTela.imagemPrincipal) {
      this.taskImagePrincipal = this.eventoService.upload(this.controleTela.imagemPrincipal);
    }

    if (this.controleTela.imagemDetalhe) {
      this.taskImageDetalhe = this.eventoService.upload(this.controleTela.imagemDetalhe);
    }

    Promise.all([this.taskImagePrincipal, this.taskImageDetalhe]).then(results => {
      let downloadUrlPrincipal: Promise<any>;
      let downloadUrlDetalhe: Promise<any>;

      if (results[0] && results[0].metadata.fullPath) {
        downloadUrlPrincipal = results[0].ref.getDownloadURL();
      }

      if (results[1] && results[1].metadata.fullPath) {
        downloadUrlDetalhe = results[1].ref.getDownloadURL();
      }

      Promise.all([downloadUrlPrincipal, downloadUrlDetalhe]).then(urls => {
        if (urls[0]) {
          this.evento.downloadUrlImagePrincipal = urls[0];
        }
        if (urls[1]) {
          this.evento.downloadUrlImageDetail = urls[1];
        }

        this.getEvento(this.formEvento, this.evento);
        const evento = Object.assign({}, this.evento);

        evento.manPrice = Number(evento.manPrice);
        evento.womanPrice = Number(evento.womanPrice);

        evento.date = Number(evento.date);
        evento.dateTimeBegin = Number(evento.dateTimeBegin);
        evento.dateTimeExhibition = Number(evento.dateTimeExhibition);

        const idsCategorias = this.categoriasBase.filter(categoria => {
          if (evento.category.indexOf(categoria.name) >= 0) {
            return categoria;
          }
        });
        evento.idsCategory = idsCategorias.map(cat => cat.id);

        this.eventoService.update(evento).then(() => {
          this.message.open('Evento '
            + this.evento.name + ' '
            + (this.evento.id ? 'alterado' : 'criado')
            + ' com sucesso', 'Ok', { duration: 5000 });

          this.router.navigate(['/menu/evento']);
        });
      });
    });
  }

}
