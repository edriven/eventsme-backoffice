import { CadastroEventoComponent } from './cadastro-evento/cadastro-evento.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventoComponent } from './evento/evento.component';

const routes: Routes = [
  {
    path: '',
    component: EventoComponent
  },
  {
    path: 'edit',
    component: CadastroEventoComponent
  },
  {
    path: 'edit/:id',
    component: CadastroEventoComponent
  },
  {
    path: 'new',
    component: CadastroEventoComponent
  },
  {
    path: 'new/:order',
    component: CadastroEventoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventosRoutingModule { }
