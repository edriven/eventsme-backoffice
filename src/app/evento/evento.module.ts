import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { EventosRoutingModule } from './evento-routing.module';
import { GridEventoComponent } from './grid-evento/grid-evento.component';
import { CadastroEventoComponent } from './cadastro-evento/cadastro-evento.component';
import { EventoComponent } from './evento/evento.component';

@NgModule({
  declarations: [
    GridEventoComponent,
    CadastroEventoComponent,
    EventoComponent
  ],
  imports: [
    SharedModule,
    EventosRoutingModule,
  ],
  providers: []
})
export class EventoModule { }
