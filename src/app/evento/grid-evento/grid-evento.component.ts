import { EventoService } from './../../http/evento.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource, SimpleSnackBar, MatSnackBarRef, MatSnackBar, MatSort } from '@angular/material';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CategoriaService } from 'src/app/http/categoria.service';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Evento } from 'src/app/model/evento';

@Component({
  selector: 'app-grid-evento',
  templateUrl: './grid-evento.component.html'
})

export class GridEventoComponent implements OnInit {
  public colunas: string[] = ['select', 'name', 'category', 'dateTimeBegin', 'city'];
  public eventos;
  public showOnlyEventsNotFinish: Boolean = true;
  private allEventos: [{order, dateTimeBegin}];
  public eventosOrdenar;
  private selection = new SelectionModel(true, []);
  private snackBarRef: MatSnackBarRef<SimpleSnackBar>;
  private formFiltro: FormGroup;
  public mask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public modoOrdenar = false;
  public nextOrder: number;

  @ViewChild(MatSort) sort: MatSort;
  categorias: Observable<any>;

  constructor(
    private eventoService: EventoService,
    private snackBar: MatSnackBar,
    private categoriaService: CategoriaService
  ) {
    this.formFiltro = new FormGroup({
      category: new FormControl(''),
      date: new FormControl(''),
      city: new FormControl('')
    });
  }

  ngOnInit() {
    this.listar();

    this.formFiltro.valueChanges.pipe(
      debounceTime(800)
    ).subscribe(val => {
      let busca = val;
      if (busca.date) {
        busca.date = new Date(busca.date).getTime().toString();
      }
      this.listar(busca);
    });

    this.categorias = this.categoriaService.getCategorias();
  }

  drop(event: CdkDragDrop<string[]>) {
    const eventosUpdate: Array<Evento> = [];
    const eventoDestino = this.eventos.data.filter(evento => {
      console.log(evento.name+" evento.order "+evento.order+" "+event.currentIndex); 
      if (evento.order === event.currentIndex) {
        return evento;
      }
    });
    const eventosMovidos = this.eventos.data.filter(evento => {
      console.log(evento.name+" evento.order "+evento.order+" "+event.previousIndex); 
      if (evento.order === event.previousIndex) {
        return evento;
      }
    });

    if (eventoDestino !== null) {
      if (event.previousIndex < event.currentIndex) {
        this.eventos.data.forEach(evento => {
          if (evento.order > event.previousIndex && evento.order <= event.currentIndex && evento.order !== 0) {
            evento.order = evento.order - 1;
            eventosUpdate.push(evento);
          }
        });
      } else {
        this.eventos.data.forEach(evento => {
          if (evento.order < event.previousIndex && evento.order >= event.currentIndex) {
            evento.order = evento.order + 1;
            eventosUpdate.push(evento);
          }
        });
      }
      eventosUpdate.forEach(eventoUpdate => {
        this.eventoService.update(eventoUpdate);
      });
    }

    eventosMovidos.forEach(element => {
      element.order = event.currentIndex;
      this.eventoService.update(element);
    });

  }

  limparFiltros() {
    const object = this.formFiltro.controls;

    for (const key in object) {
      if (object.hasOwnProperty(key)) {
        const element = object[key];
        element.setValue('');
      }
    }
  }

  listar(busca?) {
    this.eventoService.listar(busca).subscribe(eventos => {
      //Edilson Ribeiro 08/08/2019
      this.allEventos = eventos;
      if (this.showOnlyEventsNotFinish) {
        eventos = eventos.filter((evento) => {
          return evento.dateTimeBegin > (new Date().getTime());
        })
      }
      this.eventos = new MatTableDataSource(eventos);
      this.eventosOrdenar = new MatTableDataSource(eventos);
      this.ordenarLista();
    });
  }

  //Edilson Ribeiro 08/08/2019
  ordenarLista() {
    if (this.allEventos) {
      this.eventos.sort = this.sort;
      let orderAux = 0;
      this.allEventos.forEach(({order}) => {
        if (order > 0) {
          orderAux = order;
        }
      });
      this.nextOrder = orderAux + 1;
    }
  }

  //Edilson Ribeiro 08/08/2019
  showOnlyEventsNotFinishClick() {
    if (this.showOnlyEventsNotFinish) {
      let eventos = this.allEventos.filter(({ dateTimeBegin }) => {
        return dateTimeBegin > (new Date().getTime());
      })
      this.eventos = new MatTableDataSource(eventos);
      this.eventosOrdenar = new MatTableDataSource(eventos);
    } else {
      this.eventos = new MatTableDataSource(this.allEventos);
      this.eventosOrdenar = new MatTableDataSource(this.allEventos);
    }
    //this.showOnlyEventsNotFinish = !this.showOnlyEventsNotFinish;
    this.ordenarLista();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.eventos.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.eventos.data.forEach(row => this.selection.select(row));
  }

  delete() {
    this.snackBarRef = this.snackBar.open('Confirma exclusão dos eventos selecionadas?', 'Confirmar', { duration: 5000 });

    this.snackBarRef.onAction().subscribe(() => {
      this.selection.selected.forEach(evento => {
        evento.published = true;
        let eventosAlterar: Array<Evento> = [];

        this.eventoService.delete(evento).then(() => {
          const eventosMaiores = this.eventos.data.filter(eventoFilter => {
            if (eventoFilter.order > evento.order) {
              return eventoFilter;
            }
          });
          eventosMaiores.forEach(eventoMaior => {
            eventoMaior.order = (eventoMaior.order - 1);
            eventosAlterar.push(eventoMaior);
          });
          eventosMaiores.forEach(eventoAlterar => {
            this.eventoService.update(eventoAlterar);
          });
          this.snackBar.open('Evento removido com sucesso!', 'OK', { duration: 5000 });
          this.selection.clear();
        });
      });
    });
  }


}
