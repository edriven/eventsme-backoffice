import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Evento } from '../model/evento';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class EventoService {

  constructor(
    private db: AngularFirestore,
    private storage: AngularFireStorage
  ) { }

  listar(busca?): Observable<any> {
    return this.db.collection('eventos', ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      if (busca) {
        for (const key in busca) {
          if (busca.hasOwnProperty(key)) {
            const element = busca[key];
            if (element) {
              query = query.where(key, '==', element);
            }
          }
        }
      }
      query = query.orderBy('order', 'asc');
      return query;
    }).valueChanges();
    /* .pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data};
        });
      })
    ); */
  }

  update(evento: Evento): Promise<any> {
    if (!evento.id) {
      evento.id = this.db.createId();
      return this.db.collection('eventos').doc<Evento>(evento.id).set(evento);
    } else {
      return this.db.collection('eventos').doc<Evento>(evento.id).update(evento);
    }
  }

  upload(arquivo, id?): AngularFireUploadTask {
    if (!id) {
      id = this.db.createId();
    }
    return this.storage.ref('/eventos/' + id + '/' + arquivo.name).put(arquivo);
  }
  
  obter(idEvento: string): Observable<Evento> {
    return this.db.collection('eventos').doc<Evento>(idEvento).valueChanges();
  }

  delete(evento: Evento): Promise<any> {
    return this.db.collection('eventos').doc(evento.id).delete();
  }
}