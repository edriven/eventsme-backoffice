import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  constructor(
    private db: AngularFirestore
  ) { }

  obter(id): Observable<any>{
    return this.db.collection('users').doc(id).valueChanges();
  }

  listar(nome?): Observable<any>{
    return this.db.collection('users', ref =>{
      let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      if (nome) { query = query.where('nome', '==', nome) };
      return query;
    }).snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

}
