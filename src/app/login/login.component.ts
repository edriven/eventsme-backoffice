import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../http/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public usuario;
  public senha;
  public exception;

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.loginService.logout();
  }

  login(){
    this.exception = null;
    this.loginService.login(this.usuario, this.senha).then(usuario =>{
        this.loginService.isAdmin(usuario.user.uid).then(isAdmin =>{
            if(isAdmin){
                this.router.navigate(['/menu']);
            } else {
                this.exception = 'Usuário não autorizado.'
                this.loginService.logout();
            }
        });
    })
    .catch(exception =>{
        this.exception = 'E-mail ou senha incorretos.'
    });
  }


}
