import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu.component';

const routes: Routes = [
  {
    path:'', 
    component:MenuComponent, 
    children: [
      {
        path:'', 
        redirectTo:'evento', 
        pathMatch:'full'
      },
      {
        path:'evento', 
        loadChildren:'../evento/evento.module#EventoModule'
      },
      {
        path:'usuario', 
        loadChildren:'../usuario/usuario.module#UsuarioModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuRoutingModule { }
