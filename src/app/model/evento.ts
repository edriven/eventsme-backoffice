export class Evento {
    id: string;
    name: string;
    category: String[];
    idsCategory: String[];
    description: string;
    urlPurchase: string;
    urlMarketing: string;
    dateTimeBegin: number;
    date: number;
    address: string;
    city: string;
    uf: string;
    downloadUrlImagePrincipal: string;
    downloadUrlImageDetail: string;
    showDetails: boolean;
    showIndex: boolean;
    marketing: boolean;
    suggested: boolean;
    active: boolean;
    dateTimeExhibition: number;
    principalImageProportion: string;
    detailImageProportion: string;

    order: number;
    manPrice: number;
    womanPrice: number;

    eventsmeUrl: string;
    eventsmeValue: number;
    partnerUrl: string;
    partnerValue: number;
    stubhubUrl: string;
    stubhubValue: number;
    ticketswapUrl: string;
    ticketswapValue: number;
    
}
