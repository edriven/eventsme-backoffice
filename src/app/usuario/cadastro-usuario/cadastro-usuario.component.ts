import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { UsuarioService } from 'src/app/http/usuario.service';

@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html'
})
export class CadastroUsuarioComponent implements OnInit {
  public usuario: any;

  constructor(
    private route: ActivatedRoute,
    private usuarioService: UsuarioService
  ) { }

  ngOnInit() {
    this.usuario = {};

    this.route.params.subscribe(params => {
      this.usuarioService.obter(params['id']).subscribe(document => {
        this.usuario = document;
      });
    });
  }


}
