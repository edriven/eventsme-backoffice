import { UsuarioService } from './../../http/usuario.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { ExcelService } from 'src/app/services/excel.service';

@Component({
  selector: 'app-grid-usuario',
  templateUrl: './grid-usuario.component.html'
})
export class GridUsuarioComponent implements OnInit {
  public colunas: string[] = ['nome', 'email'];
  public usuarios = new MatTableDataSource();
  private campoBusca: FormControl;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private usuarioService: UsuarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private excelService: ExcelService
  ) {
    this.campoBusca = new FormControl('');
  }

  ngOnInit() {
    this.listar();

    this.campoBusca.valueChanges.pipe(
      debounceTime(800)
    ).subscribe(val => {
      this.listar(val);
    });
  }

  limparFiltros() {
    this.campoBusca.setValue("");
  }

  listar(busca?) {
    this.usuarioService.listar(busca).subscribe(usuarios => {
      this.usuarios = new MatTableDataSource(usuarios);
      this.usuarios.sort = this.sort;
    });
  }

  selecionarLinha(linha: any) {
    this.router.navigate(['cadastro/'+ linha.id], {relativeTo: this.activatedRoute});
  }

  exportarUsuarios() {
    this.usuarioService.listar().subscribe(usuariosJson => {
        this.excelService.exportAsExcelFile(usuariosJson, 'Usuarios');
    });
}

}
