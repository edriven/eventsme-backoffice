import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioComponent } from './usuario.component';
import { GridUsuarioComponent } from './grid-usuario/grid-usuario.component';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';


@NgModule({
  declarations: [
    UsuarioComponent,
    GridUsuarioComponent,
    CadastroUsuarioComponent
  ],
  imports: [
    SharedModule,
    UsuarioRoutingModule,
  ]
})
export class UsuarioModule { }
