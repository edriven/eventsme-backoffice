// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBYmxEiNQsiqvaq7ifklkEPVTXDGO2_Cek",
    authDomain: "eventsme-1181b.firebaseapp.com",
    databaseURL: "https://eventsme-1181b.firebaseio.com",
    projectId: "eventsme-1181b",
    storageBucket: "eventsme-1181b.appspot.com",
    messagingSenderId: "140201438369"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
